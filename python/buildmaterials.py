"""
	Build the JSON files required by the wiki gamedata.js
	The master source file is gamedata.json that ships with the game
	3 JSON files are required by the wiki gamedata.js
		materials.json		- Cases,
								Modules,
								Components,
								Base Materials,
								special, and
								Campaign specific items
		techs.json			- The 'Research Tree'
		equipment.json		- Tables, Machines, Decoratives
		
	***
	
	This is a work in progress.
	
	Desired end state is to generate the other 3 files from gamedata.json
	
	***
	
	This version
		- Generates materials.json wholly from gamedata.json
		
	***
"""

# Required json modules
from cgi import print_arguments
import json
import datetime

# Load the source data
f = open("../gamedb/gamedata/gamedata.json", "rt")
gamedata = json.loads(f.read())
f.close()

# Allocate some arrays for each item to enrich materials.json

ModulesByMachine = {} 		# for the 'crafted_by' keys
ModulesAsIngredients = {}	# for the 'used_to_produce' keys 
ModulesByAnalysis = {}		# for the 'analysed_by' key

# Materials contains all the modules and the raw materials
# Get them all
# The ID is unique
MaterialLookup = {}
for Material in gamedata["Materials"]:
	MaterialLookup[int(Material)] = gamedata["Materials"][Material]["Name"]

# Modules contains all the module names and their unique IDs
# It doesn't include the Materials
# The material IDs and Module IDs are not the same
# Get them all
ModuleLookup = {}
for Module in gamedata["Modules"]:
	ModuleLookup[int(Module)] = gamedata["Modules"][Module]["Name"]

# Get the table/machine names
MachineLookup = {}
for Machine in gamedata["IconProperties"]:
	MachineLookup[int(Machine)] = gamedata["ObjectTypes"][Machine]["Name"]

# Get List of what machine can make what, and how long it takes
UsedToProduceAnalyseShapeTierCraftedWith = {}
for Machine in gamedata["CrafterProperties"]:
	if "PossibleModules" in gamedata["CrafterProperties"][Machine].keys():
		for Module in gamedata["CrafterProperties"][Machine]["PossibleModules"]:
			if ModuleLookup[Module["ModuleID"]] not in UsedToProduceAnalyseShapeTierCraftedWith.keys():
				UsedToProduceAnalyseShapeTierCraftedWith[ModuleLookup[Module["ModuleID"]]] = []				
			UsedToProduceAnalyseShapeTierCraftedWith[ModuleLookup[Module["ModuleID"]]].append({"table_id" : MachineLookup[int(Machine)], "craft_duration": Module["Duration"] / gamedata["TuningValues"]["SecondsPerDay"]})

# Build details of the modules/base materials used to make other things
# and what those things are
# If the Module fits in a case, calculate a filename
# for the module shape
# Also gather the research tier for anything that is researchable

for Module in gamedata["Modules"]:
	if "InputMaterials" in gamedata["Modules"][Module]:
		for InputMaterial in gamedata["Modules"][Module]["InputMaterials"]:
			MaterialName = MaterialLookup[InputMaterial["MaterialID"]]
			ModuleName = ModuleLookup[int(Module)]
			if MaterialName not in UsedToProduceAnalyseShapeTierCraftedWith.keys():
				UsedToProduceAnalyseShapeTierCraftedWith[MaterialName] = []				
			UsedToProduceAnalyseShapeTierCraftedWith[MaterialName].append({"make_id": ModuleName})
	if "GridDimensions" in gamedata["Modules"][Module]:
		filename = ""
		for g in gamedata["Modules"][Module]["GridDimensions"]:
			filename = filename + str(g["X"]) + str(g["Y"])
		if filename != "":
			ModuleName = ModuleLookup[int(Module)]
			if ModuleName not in UsedToProduceAnalyseShapeTierCraftedWith.keys():
				UsedToProduceAnalyseShapeTierCraftedWith[ModuleName] = []				
			UsedToProduceAnalyseShapeTierCraftedWith[ModuleName].append({"module_shape" : filename})
	if "Tier" in gamedata["Modules"][Module]:
		ModuleName = ModuleLookup[int(Module)]
		if ModuleName not in UsedToProduceAnalyseShapeTierCraftedWith.keys():
			UsedToProduceAnalyseShapeTierCraftedWith[ModuleName] = []				
		UsedToProduceAnalyseShapeTierCraftedWith[ModuleName].append({"Tier": gamedata["Modules"][Module]["Tier"] + 1})
	
# ResearchAndDevelopmentProperties tells us what modules
# can be used in which analysis tables
# Grab this to include in materials.json

ResearchAndDevelopmentProperties = gamedata["ResearchAndDevelopmentProperties"]
for i in ResearchAndDevelopmentProperties:
	PossibleModules = ResearchAndDevelopmentProperties[i]["PossibleModules"]
	if ResearchAndDevelopmentProperties[i]["CanDoResearch"]:
		for listitem in PossibleModules:
			ModuleName = ModuleLookup[int(listitem)]
			if ModuleName not in UsedToProduceAnalyseShapeTierCraftedWith.keys():
				UsedToProduceAnalyseShapeTierCraftedWith[ModuleName] = []				
			UsedToProduceAnalyseShapeTierCraftedWith[ModuleName].append({"analysed_by": MachineLookup[int(i)]})

# Need the modulekeys to join with the materials
ModuleKeys = {}
NewMaterials = {}
NewMaterials = {"version" : "1.0"}
NewMaterials["materials"] = []
for Module in ModuleLookup:
	ModuleKeys[ModuleLookup[Module]] = Module
for Material in MaterialLookup:
	ModulePrototype = {}
	MaterialName = MaterialLookup[Material]
	MaterialID = str(Material)
	# Is this a raw material or a module/component/case?
	if MaterialName in ModuleKeys.keys():
		# This is a module		
		ModID = str(ModuleKeys[MaterialName])
	else:
		# there is no module that matches this material
		ModID = ""
	if ModID != "":
		# Modules, Components, Cases (not base materials
		for input_material in gamedata["Modules"][ModID]["InputMaterials"]:
			if "input_materials" not in ModulePrototype.keys():
				ModulePrototype["input_materials"] = []
			Material = gamedata["Materials"][str(input_material["MaterialID"])]
			ModulePrototype["input_materials"].append({
				"icon_sprite": Material["TextSpriteAssetID"],
				"icon_id": Material["Name"],
				"loca_string": Material["Name"],
				"material_id": input_material["MaterialID"],
				"material_amount": input_material["Amount"]
			})
		ModulePrototype["sampling_time"] = gamedata["Modules"][ModID]["SamplingTime"] / gamedata["TuningValues"]["SecondsPerDay"]
		# Forget module_fields now we have filename
		ModulePrototype["material_id"] = gamedata["Modules"][ModID]["MaterialID"]
		ModulePrototype["sell_price"] = gamedata["Modules"][ModID]["BaseMarketPrice"]
		ModulePrototype["output_amount"] = gamedata["Modules"][ModID]["OutputAmount"]
		"""
			NEED TO RESOLVE RESEARCH DATA AMOUNTS
		"""
		if "data_amounts" not in ModulePrototype.keys():
			ModulePrototype["data_amounts"] = []
		if "ResearchDataYield" in gamedata["Modules"][ModID]:
			for ResearchYield in gamedata["Modules"][ModID]["ResearchDataYield"]:
				ModulePrototype["data_amounts"].append({
						"data_amount": ResearchYield["Amount"],
						"icon_sprite": "icons_researchdata",
						"icon_id": gamedata["ResearchDataTypes"][str(ResearchYield["DataTypeID"])]["Name"],
						"loca_string": gamedata["ResearchDataTypes"][str(ResearchYield["DataTypeID"])]["Name"],
						"data_id": ResearchYield["DataTypeID"]
				})
		"""
			NEED TO RESOLVE MODULE FEATURES
		"""
		if "module_features" not in ModulePrototype.keys():
			ModulePrototype["module_features"] = []
		if "Features" in gamedata["Modules"][ModID]:
			for Feature in gamedata["Modules"][ModID]["Features"]:
				ModulePrototype["module_features"].append({
					"feature_id": Feature["FeatureID"], 
                    "icon_sprite": "icons_features",
					# strip out 'feat_'
                    "icon_id": gamedata["ProductFeatures"][str(Feature["FeatureID"])]["Name"][5:], 
                    "loca_string": gamedata["ProductFeatures"][str(Feature["FeatureID"])]["Name"],
                    "feature_value": Feature["Value"] / 10
				})
		ModulePrototype["module_id"] = int(ModID)
		ModulePrototype["order_in_category"] = gamedata["Modules"][ModID]["OrderInCategory"]
		ModulePrototype["assembly_time"] = gamedata["Modules"][ModID]["AssemblyTime"] / gamedata["TuningValues"]["SecondsPerDay"]
		if "module_fields" not in ModulePrototype.keys():
			ModulePrototype["module_fields"] = []
		for appendage in UsedToProduceAnalyseShapeTierCraftedWith[ModuleLookup[int(ModID)]]:
			if "Tier" in appendage.keys():
				ModulePrototype["Tier"] = appendage["Tier"]
			elif "module_shape" in appendage.keys():
				ModulePrototype["module_fields"] = gamedata["Modules"][ModID]["GridDimensions"]
				ModulePrototype["module_shape"] = appendage["module_shape"]
			elif "analysed_by" in appendage.keys():
				ModulePrototype["analysed_by"] = appendage["analysed_by"]
			elif "table_id" in appendage.keys():
				if "crafted_with" not in ModulePrototype.keys():
					ModulePrototype["crafted_with"] = []
				ModulePrototype["crafted_with"].append(appendage)
		ModulePrototype["module_category"] = gamedata["ModuleCategories"][str(gamedata["Modules"][ModID]["CategoryID"])]["Name"]
	else:
		# Unique to base materials
		ModulePrototype["material_id"] = int(MaterialID)
		ModID = MaterialID
		ModulePrototype["module_category"] = "cat_material"
		
	#Common for base materials, modules, components, cases
	ModulePrototype["loca_string"] = gamedata["Materials"][MaterialID]["Name"]
	ModulePrototype["icon_id"] = gamedata["Materials"][MaterialID]["Name"]
	ModulePrototype["icon_sprite"] = gamedata["Materials"][MaterialID]["TextSpriteAssetID"]
	for appendage in UsedToProduceAnalyseShapeTierCraftedWith[MaterialLookup[int(MaterialID)]]:
		if "make_id" in appendage.keys():
			if "used_to_produce" not in ModulePrototype.keys():
				ModulePrototype["used_to_produce"] = []
			ModulePrototype["used_to_produce"].append(appendage)

	ModulePrototype["stack_buy_price"] = gamedata["Materials"][MaterialID]["StackBuyPrice"]
	ModulePrototype["stack_size"] = gamedata["Materials"][MaterialID]["StackSize"]
	
	NewMaterials["materials"].append(ModulePrototype)
		
#"""
#	NewMaterials is built. Before we write it, make sure it contains everything we would expect
#	by comparing it with the previous version
#	Debug code. Commented Out
#"""
#f = open("../gamedb/gamedata/materials.json", "rt")
#OldMaterials = json.loads(f.read())
#f.close()

#if OldMaterials.keys() != NewMaterials.keys():
#	print("*** Old and New Materials Mismatch ***")
#	exit()
#NewKeys = {}
#for NewM in NewMaterials["materials"]:
#	NewKeys[NewM["material_id"]] = NewM
#OldKeys = {}
#for OldM in OldMaterials["materials"]:
#		OldKeys[OldM["material_id"]] = OldM
#for OldKey in OldKeys.keys():
#	if OldKey not in NewKeys.keys():
#		print("*** Old and New Materials IDs Mismatch ***: " + str(OldKey))
#		exit()

## All materials and modules present, now check contents
## Sample each section type, not everything
#mustexit = False
#for OldKey in OldKeys.keys():
#	OldNext = OldKeys[OldKey]
#	inputmaterials = False
#	noinputmaterials = False
#	usedtoproduce = False
#	craftedwith = False
#	dataamounts = False
#	analysedby = False
#	NewNext = NewKeys[OldKey]
#	if OldNext.keys() != NewNext.keys():
#		print("*** Keys for Old Material Mismatch ***: " +str(OldKey))
#		for WrongKey in OldNext.keys():
#			if WrongKey not in NewNext.keys():
#				print("*** Old Key Missing ***: " + str(WrongKey))
#				mustexit = True
#		for WrongKey in NewNext.keys():
#			if WrongKey not in OldNext.keys():
#				print("*** New Key Added ***: " + str(WrongKey))
#		if mustexit:
#			exit()
#	"""
#		Still going, Compare the old fields with the new
#	"""
#	for NextOldKey in OldNext:
#		if OldNext[NextOldKey] != NewNext[NextOldKey]:
#			print(NextOldKey)

# Write the materials.json
# to avoid corrupting the original file
# make a backup

f = open("../gamedb/gamedata/materials.json", "rt")
backup = (f.read())
f.close()
# Create the backup
stamp = datetime.datetime.now()
stampstr = stamp.strftime("../archive/materials%Y%m%d-%H%M%S.json")
f = open(stampstr, "xt")
f.write(backup)
f.close()

# Write the new file

j = json.dumps(NewMaterials, indent = 4)
f = open("../gamedb/gamedata/materials.json", "w")
f.write(j)
f.close()