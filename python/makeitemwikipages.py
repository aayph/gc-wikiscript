#!/usr/bin/python3

# import required Python libraries
import json
import requests
import time

"""
	Build the wikitext for the items
	Use the MediaWiki API to edit the pages
	
	CaseTemplate
	---- 1 - The icon_id for the module
	---- 2 - the number of slots in the case/chassis
	---- 3 - The text description from the language files (currently commented out in the wiki template, but sent anyway)
	---- 4 - The shape of the case and the slots (not included in the layout at present)
	
	Component Template
	---- 1 - The icon_id for the module
	
	Module Template
	---- 1 - The icon_id for the module
	
		Material Template
	---- 1 - The icon_id for the base material
	
"""

# Authenticate with the Wiki
S = requests.Session()

URL = "https://wiki.goodcompanygame.com/api.php"

# Step 1: GET Request to fetch login token
PARAMS_0 = {
    "action": "query",
    "meta": "tokens",
    "type": "login",
    "format": "json"
}

R = S.get(url=URL, params=PARAMS_0)
DATA = R.json()
# print(DATA)


LOGIN_TOKEN = DATA['query']['tokens']['logintoken']

# Step 2: POST Request to log in. Use of main account for login is not
# supported. Obtain credentials via Special:BotPasswords
# (https://www.mediawiki.org/wiki/Special:BotPasswords) for lgname & lgpassword

# Load the Bot credentials
# THIS FILE IS EXCLUDED FROM THE GITLAB REPOSITORY
# with an entry in .gitignore

f = open("./credentials.txt", "rt")
credentials = json.loads(f.read())
f.close()

PARAMS_1 = {
    "action": "login",
    "lgname": credentials["username"],
    "lgpassword": credentials["password"],
    "lgtoken": LOGIN_TOKEN,
    "format": "json"
}

R = S.post(URL, data=PARAMS_1)
DATA = R.json()
# print(DATA)

# Build the Component, Module, and Case wiki pages
f = open("../gamedb/gamedata/materials.json" , "rt")
Materials = json.loads(f.read())
f.close()
f = open("../gamedb/localization/goodcompanyBase_en.json" , "rt")
language = json.loads(f.read())
f.close()
Materials = Materials["materials"]

f = open("../gamedb/gamedata/gamedata.json" , "rt")
gamedata = json.loads(f.read())
f.close()

wikitextpreamble = """<!--
	DO NOT PLACE PAGE CONTENT ABOVE THIS COMMENT BLOCK.
	The templates below generate an infobox on the RHS of the page
	PLEASE DO NOT REMOVE THEM!
	Start any more content below the next comment block
-->
__NOTOC__"""
wikitextpostamble = """
<!--
	ADD YOUR CONTENT BELOW THIS COMMENT 
-->"""
for Material in Materials:
	"""
		Materials use Materials template
		Case or Chassis use Case template
		Component and event use Component template
		Everything else use Module template
		
		what we do depends on the "module_category"
	"""
	# print(type(Material))
	# print(Material.keys())
	# print(Material)
	# print(Material)
	wikitextInfo = ""
	wikitextRecipe = ""
	wikitextUsedIn = ""
	wikitextModulefeatures = ""
	wikitextAnalysis = ""
	wikitextResearch = ""
	wikitextNeedsUnlocking = ""
	ItemType = Material["module_category"]
	ItemName = Material["icon_id"]
	ItemDescription = gamedata["Materials"][str(Material["material_id"])]["Description"]
	if ItemType == "cat_mod_case" or ItemType == "cat_mod_bot_chassis" or ItemType == "cat_mod_medium_chassis":
		wikitextInfo = "{{CaseInfoTable|[ITEMNAME]|[SLOTS]|[ITEMDESCRIPTION]||}}"
		wikitextInfo = wikitextInfo.replace("[ITEMDESCRIPTION]",ItemDescription)
		Slots = language[ItemDescription]
		# *** NASTY HARDCODING ***
		# Is string of form "*HAS nn SLOTS* ..."
		Slots = Slots[5:7]
		wikitextInfo = wikitextInfo.replace("[SLOTS]",Slots)		
	elif ItemType == "cat_mod_components":
		wikitextInfo = "{{ComponentInfoTable|[ITEMNAME]}}"
	elif ItemType == "cat_material":
		wikitextInfo = "{{MaterialInfoTable|[ITEMNAME]}}"
	else:
		# print(Material)
		# print(ItemName)
		# print(Material.keys())
		wikitextInfo = "{{ModuleInfoTable|[ITEMNAME]}}"

	if wikitextInfo == "":
		print("==== UNKNOWN MATERIAL TYPE ====")
		print(Materials[Material])
		exit()

	# Everything has a recipe except the raw materials
	if ItemType != "cat_material":
		wikitextRecipe = "\n{{RecipeTemplate|[ITEMNAME]}}"

	# Can this be used to produce anything else?
	if "used_to_produce" in Material.keys():
		if len(Material["used_to_produce"]) > 0:
			wikitextUsedIn = "\n{{UsedInTemplate|[ITEMNAME]}}"

	if ItemType != "cat_mod_components" and ItemType != "cat_material":
		# Is case or module, add the features
		if "module_features" in Material.keys():
			if len(Material["module_features"]) > 0:
				wikitextModulefeatures = "\n{{ModuleFeaturesTemplate|[ITEMNAME]}}"
		# Is this analysed on a table? Some special modules are not
		if "analysed_by" in Material.keys():
			if len(Material["analysed_by"]) > 0:
				wikitextAnalysis = "\n{{AnalysisTemplate|[ITEMNAME]}}"

		# Is this researchable? Some special modules are not
		# If it is researchable, does something else have to be researched first?
		# We don't have anything in materials.json
		# need to use gamedata "DevelopmentProjects"
		# gamedata["DevelopmentProjects"][]
		# or techs.json
		# Neither of these are easy to cross reference

		# Special Modules are not researchable
		if Material["module_category"] != "cat_mod_special":
			wikitextResearch = "\n{{ResearchTemplate|[ITEMNAME]|}}"

	pagename = language[ItemName] + " (item)"
	wikitext = wikitextInfo + wikitextRecipe + wikitextUsedIn + wikitextModulefeatures + wikitextAnalysis + wikitextResearch
	wikitext = wikitext.replace("[ITEMNAME]",ItemName)
	# print(wikitext)
	wikitext = wikitextpreamble + wikitext + wikitextpostamble

	"""
		Example for future development to retain page content
		https://wiki.goodcompanygame.com/api.php?action=query&prop=revisions&titles=pagename&rvslots=*&rvprop=content&formatversion=2&format=json

		We should use the  the pagename to get all the content after wikitextpostamble, and add it back on before posting
	"""

	# Step 3: GET request to fetch CSRF token
	PARAMS_2 =	{
					"action": "query",
					"meta": "tokens",
					"format": "json"
				}

	R = S.get(url=URL, params=PARAMS_2)
	DATA = R.json()
	# print(DATA)

	CSRF_TOKEN = DATA['query']['tokens']['csrftoken']

	# Step 4: POST request to edit a page
	PARAMS_3 =	{
					"action": "edit",
					"title": pagename,
					"token": CSRF_TOKEN,
					"format": "json",
					"text": wikitext,
					"bot": True
				}

	R = S.post(URL, data=PARAMS_3)
	DATA = R.json()
	# print(DATA)

	# If this is the first time the page is being created
	# then repost with the captcha information needed for a new page
	if 'edit' in DATA.keys():
		if 'result' in DATA['edit'].keys():
			if DATA['edit']['result'] == 'Failure':
				time.sleep(2) # Avoid flooding the server
				PARAMS_3["captchaid"] = DATA['edit']['captcha']['id']
				PARAMS_3["captchaword"] = "Good Company"
				R = S.post(URL, data=PARAMS_3)
				DATA = R.json()
	else:
		# Unknown error. May be caused by session and/or CSRF token expiry
		print(json.dumps(DATA), indent = 4)
		exit()
			
	# Wait for 2 seconds before building the next page
	print("Posted : " + pagename)
	time.sleep(2)

# LOGOFF ??